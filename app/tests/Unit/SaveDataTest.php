<?php

namespace Tests\Unit;

use App\Models\Data;
use App\Services\SaveData;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SaveDataTest extends TestCase
{
    use RefreshDatabase;

    public function testInit()
    {
        // valid json
        $json = '{"key": "value"}';
        $dataService = new SaveData($json);
        $this->assertIsArray($dataService->getData());

        // invalid json
        $this->expectException(\JsonException::class);
        $wrong_json = '{key, "key2": "value"}';
        $dataService = new SaveData($wrong_json);

        // not json
        $this->expectException(\TypeError::class);
        $not_json = ['key' => 'value'];
        $dataService = new SaveData($not_json);

        // the data is collected
        $request = factory(Data::class)->raw();
        $dataService = new SaveData(
            to_json($request)
        );
        $this->assertEquals($request, $dataService->getData());
    }

    public function testSave()
    {
        // data saved
        $request = factory(Data::class)->raw();
        $dataService = new SaveData(
            to_json($request)
        );
        $result = $dataService->sp_SaveData();

        $this->assertTrue($dataService->isSuccessResult());
        $this->assertFalse($dataService->isFailedResult());
        $this->assertDatabaseHas('data', $request);

        // even or odd
        $this->assertEquals(SaveData::ODD_AMOUNT, $result);

        $request = factory(Data::class)->raw();
        $dataService = new SaveData(
            to_json($request)
        );
        $result = $dataService->sp_SaveData();
        $this->assertEquals(SaveData::EVEN_AMOUNT, $result);
    }

}
