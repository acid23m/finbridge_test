(function (d, w) {
    'use strict';

    // color chooser
    // --------------------------

    let colorInput = d.querySelector('.js-color-input');
    let colorResult = d.querySelector('.js-color-result');

    function colorChange (input) {
        let key = parseInt(input.value);

        colorResult.style.backgroundColor = w.colorMap[key];
    }

    colorChange(colorInput);

    colorInput.addEventListener('input', e => {
        let input = e.target;

        colorChange(input);
    });

    // alerts
    // --------------------------

    let alert = d.querySelector('.js-alert');
    if (alert) {
        // hide with delay
        setTimeout(function () {
            alert.classList.add('close');
        }, 5000);
    }

})(document, window);
