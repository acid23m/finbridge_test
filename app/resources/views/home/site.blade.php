<?php

use App\Models\Data;

/** @var Data[] $records */
?>

@extends('layouts.main')

@section('body')
    <main>
        <script>
            window.colorMap = {!! to_json(Data::COLOR_MAP) !!};
        </script>

        <form action="{{ route('store') }}" method="post">
            <div class="form-row color-row">
                <div class="color-box js-color-result"></div>
                <input class="js-color-input" type="range" name="color_key" min="1" max="10" step="1"
                       value="{{ old('color_key') ?? '5' }}">
            </div>
            <div class="form-row">
                <input type="text" name="email" placeholder="Эл. почта" value="{{ old('email') }}">

                @error('email')
                <div class="error">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-row">
                <input type="text" name="name" placeholder="Имя" value="{{ old('name') }}">

                @error('name')
                <div class="error">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-row">
                @csrf
                <button type="submit">Отправить</button>
            </div>
        </form>


        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Цвет</th>
                <th>Эл. почта</th>
                <th>Имя</th>
                <th>Время создания</th>
            </tr>
            </thead>

            <tbody>
            @forelse($records as $data)
                <tr>
                    <td>{{ $data->id }}</td>
                    <td style="background-color: {{ Data::COLOR_MAP[$data->color_key] }}">&nbsp;</td>
                    <td>{{ $data->email }}</td>
                    <td>{{ $data->name }}</td>
                    <td>{{ format_date($data->created_at) }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">Нет записей</td>
                </tr>
            @endforelse
            </tbody>
        </table>

        <div>
            <a href="{{ route('clear') }}">Очистить таблицу</a>
        </div>
    </main>
@endsection
