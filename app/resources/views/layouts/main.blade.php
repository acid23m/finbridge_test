<!doctype html>
<html lang="{{ \str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{--    <meta http-equiv="Content-Security-Policy" content="block-all-mixed-content">--}}
    <meta http-equiv="X-DNS-Prefetch-Control" content="on">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <base href="{{ url('/') }}">

    <title>{{ config('app.name') }}</title>

    @prepend('styles')
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link rel="dns-prefetch" href="//fonts.googleapis.com">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @endprepend
    @stack('styles')
</head>
<body>

<section class="alert-container">
    @if(session()->has('success'))
        <div class="alert success js-alert">
            {!! session()->get('success') !!}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert error js-alert">
            {!! session()->get('error') !!}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert error js-alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</section>

@yield('body')

@prepend('scripts')
    <script defer src="{{ asset('js/app.js') }}"></script>
@endprepend
@stack('scripts')

</body>
</html>
