<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Support\Carbon;

/**
 * Client Data.
 *
 * @property int $color_key
 * @property string $email
 * @property string $name
 * @property string|Carbon $created_at
 * @property string|Carbon $updated_at
 *
 * @package App\Models
 */
final class Data extends Model
{
    /**
     * Links slider steps with colors.
     */
    public const COLOR_MAP = [
        'green',
        'green',
        'green',
        'yellow',
        'yellow',
        'yellow',
        'red',
        'red',
        'black',
        'black',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'color_key',
        'email',
        'name',
    ];

}
