<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Data;

/**
 * Does work with client data.
 *
 * @package App\Services
 */
final class SaveData
{
    public const EVEN_AMOUNT = 0;
    public const ODD_AMOUNT = 1;

    /**
     * @var array
     */
    private array $data;
    /**
     * @var bool
     */
    private bool $result = false;

    /**
     * SaveData constructor.
     *
     * @param string $data JSON encoded client data
     */
    public function __construct(string $data)
    {
        $this->data = from_json($data);
    }

    /**
     * Saves data to the DB and returns number of records as even or odd amount.
     *
     * @return int 0|1
     */
    public function sp_SaveData(): int
    {
        $model = Data::create($this->data);

        $this->setResult((bool)$model);

        return Data::count() % 2 === 0 ? self::EVEN_AMOUNT : self::ODD_AMOUNT;
    }

    /**
     * @return bool
     */
    public function isSuccessResult(): bool
    {
        return $this->result;
    }

    /**
     * @return bool
     */
    public function isFailedResult(): bool
    {
        return !$this->isSuccessResult();
    }

    /**
     * @param bool $result
     */
    public function setResult(bool $result): void
    {
        $this->result = $result;
    }

    /**
     * String representation of the result.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->isSuccessResult() ? 'True' : 'False';
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

}
