<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\StoreRequest;
use App\Models\Data;
use App\Services\SaveData;
use Illuminate\Database\Connection as DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Homepage controller.
 *
 * @package App\Http\Controllers
 */
final class HomeController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return View
     */
    public function index(): View
    {
        return view('home.site', [
            'records' => Data::all(),
        ]);
    }

    /**
     * Saves form data.
     *
     * @param StoreRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRequest $request): RedirectResponse
    {
        $data = to_json($request->validated());

        $dataService = new SaveData($data);
        $even_odd = $dataService->sp_SaveData();

        $message = '<ul>';
        $message .= "<li>Результат: $dataService</li>";
        $message .= '<li>Число записей: ' . ($even_odd === SaveData::EVEN_AMOUNT ? 'четное' : 'нечетное') . '</li>';
        $message .= '</ul>';

        return redirect()
            ->route('home')
            ->with('success', $message);
    }

    /**
     * Deletes all records.
     *
     * @param DB $db
     * @return RedirectResponse
     */
    public function clear(DB $db): RedirectResponse
    {
        $db->table('data')->truncate();

        return redirect()
            ->route('home')
            ->with('success', 'Таблица очищена.');
    }

    public function phpinfo()
    {
        phpinfo();
    }

}
