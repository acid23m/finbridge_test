<?php
declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest.
 *
 * @package App\Http\Requests
 */
class StoreRequest extends FormRequest
{
    /**
     * Determines if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Gets the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'color_key' => 'required|integer',
            'email' => 'required|max:150|email:rfc',
            'name' => 'required|max:255|regex:/[a-z ]/i',
        ];
    }

}
