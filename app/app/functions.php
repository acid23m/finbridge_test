<?php declare(strict_types=1);

if (!\function_exists('to_json')) {
    /**
     * Returns the JSON representation of a value.
     *
     * @param mixed $data
     * @return false|string
     */
    function to_json($data)
    {
        return \json_encode($data, JSON_THROW_ON_ERROR, 512);
    }
}

if (!\function_exists('from_json')) {
    /**
     * Decodes a JSON string
     *
     * @param string $data
     * @return mixed
     */
    function from_json(string $data)
    {
        return \json_decode($data, true, 512, JSON_THROW_ON_ERROR);
    }
}

if (!\function_exists('format_date')) {
    /**
     * Formats datetime in app locale and timezone.
     *
     * @param DateTimeInterface $dt
     * @return string
     */
    function format_date(\DateTimeInterface $dt): string
    {
        /** @var string $timezone */
        $timezone = config('app.timezone', 'UTC');
        /** @var string $locale */
        $locale = config('app.locale', 'en');

        return $dt
            ->timezone($timezone)
            ->locale($locale)
            ->isoFormat('lll');
    }
}
