<?php declare(strict_types=1);

use App\Models\Data;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */

$factory->define(Data::class, static function (Faker $faker) {
    return [
        'color_key' => \random_int(1, \count(Data::COLOR_MAP)),
        'email' => $faker->unique()->safeEmail,
        'name' => $faker->firstName,
    ];
});
